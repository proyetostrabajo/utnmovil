
export const fromNumber = (importe: number) => {
  return importe.toLocaleString("es-ES", {
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
  });
};
export const fromNumberInteger = (importe: string, decimals = 0) => {

  const totalEntero = parseInt(importe, 10);
  return totalEntero.toLocaleString('es-ES', { minimumFractionDigits: decimals });
};


