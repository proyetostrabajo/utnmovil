import * as moment from 'moment';
export const DATE_FORMAT = 'DD-MM-YYYY';
export const DATE_FORMAT_SAVE = 'YYYY-MM-DD';
export const getTimestamp = (): string => Date.now().toString();

export const formatDate = (date: Date | string, format = DATE_FORMAT_SAVE): string => (typeof date === 'string') ?
        moment(date, DATE_FORMAT_SAVE).format(format) : moment(date).format(format);

export const fromNow = () => moment().format('YYYY-MM-DD');

export const fromNowThreeWeeksBefore  = (weeks: number) => {
  const days: number = weeks*7;
  const fromNow = moment().subtract(weeks, 'weeks').format('YYYY-MM-DD');
  return fromNow;
};

export const formatMonth = (month: string) => {
  const mesMoment = moment(month, 'MM').locale('es');
  return mesMoment.format('MMMM');
};

export const formatYear = (year: string) => {
  const yearMoment = moment(year, 'YY').locale('es');
  return yearMoment.format('YYYY');
};


export const fromNowOneWeeksAfter  = (weeks: number) => {
  const days: number = weeks*7;
  const fromNow = moment().add(weeks, 'w').format('YYYY-MM-DD');
  return fromNow;

};

export const fromNowOneWeeksBefore  = (day: number) => {
  const days: number = day*7;
  const fromNow = moment().subtract(day, 'days').format('YYYY-MM-DD');
  return fromNow;
};

export const addFechaHasta  = (date: Date| string): string =>{
  const weekday = moment().weekday();
  const addDateTo = 6-weekday;
  const fromNow2 = moment(date, DATE_FORMAT_SAVE).add(addDateTo, 'days').format('YYYY-MM-DD');
  return fromNow2;
};
export const restarFechaDesde  = (date: Date | string): string =>{
  const weekday = moment().weekday();
  const restDateFrom = weekday>1?weekday-1:0;
  const fromNow2 = moment(date, DATE_FORMAT_SAVE).subtract(restDateFrom, 'days').format('YYYY-MM-DD');
  return fromNow2;

};
