//
// const toRad = (value: number): number => value * Math.PI / 180;
// const toDeg = (value: number): number => value * 180 / Math.PI;
// export const calculateDistance = (from: Coordinates, to: Coordinates): number => {
//   // Diameter Earth Meters
//   const dMts: number = 12742 * 1000;
//   // Differences
//   const dLat: number = toRad(to.latitude - from.latitude);
//   const dLon: number = toRad(to.longitude - from.longitude);
//   const operationResult: number = 0.5 - Math.cos(dLat) / 2 +
//     (Math.cos(toRad(from.latitude)) *
//       Math.cos(toRad(to.latitude)) *
//       (1 - Math.cos(dLon)) / 2);
//   return Math.trunc(dMts * Math.asin(Math.sqrt(operationResult)));
// };
//
// export const round = (towersDistint, reviewsNumberOfTowers): number =>{
//   return Math.round(reviewsNumberOfTowers * 100 / towersDistint);
// };
//
// export const calculateDerivedPosition = (coordinates: Coordinates, radius,gradPoint): Coordinates => {
//
//   // Diameter Earth Meters
//  // const dMts: number = 12742 * 1000;
//   const dMts: number = 12742 * 1000;
//
//   const latA = toRad(coordinates.latitude);
//   const lonA = toRad(coordinates.longitude);
//   const angularDistance = radius / dMts;
//   const trueCourse = toRad(gradPoint);
//
//   const lat = toRad(Math.asin(Math.sin(latA) * Math.cos(angularDistance) +
//     Math.cos(latA) * Math.sin(angularDistance) * Math.cos(trueCourse)));
//
//   const dlon = Math.atan2(Math.sin(trueCourse) * Math.sin(angularDistance) * Math.cos(latA),
//     Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));
//
//   const lon = toRad(((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI);
//
//   const newCordinates: Coordinates = {
//     latitude: toDeg(lat),
//     longitude: toDeg(lon),
//   };
//
//   return newCordinates;
// };

// async getPhoto(){
//   if(this.componenteCamara.url!==''){
//     await this.base64.encodeFile(this.componenteCamara.url).then( (base64File: string) => {
//       this.photo = base64File.replace('data:image/*;charset=utf-8;base64,', 'data:image/png;base64,');
//     }, (err) => {
//       console.log(err);
//     });
//   }
// }
