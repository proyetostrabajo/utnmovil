import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from "./components/header/header.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { SHARED_COMPONENTS } from "./components";
import { NavigationHeaderComponent } from "./components/navigation-header/navigation-header.component";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
  ],
  declarations: [
    ...SHARED_COMPONENTS,
    NavigationHeaderComponent,
  ],
  exports: [
    ...SHARED_COMPONENTS,
    NavigationHeaderComponent,
  ],
})
export class SharedModule {
}
