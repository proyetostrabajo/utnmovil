import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-event',
  templateUrl: './card-event.component.html',
  styleUrls: ['./card-event.component.scss'],
})
export class CardEventComponent  implements OnInit {
  calendarIcon!:string;
  gpsIcon!:string;

  constructor() { }

  ngOnInit() {
    this.calendarIcon='assets/icon/materialicon-calendar_today.svg';
    this.gpsIcon='assets/icon/materialicon-location_on.svg';
  }

}
