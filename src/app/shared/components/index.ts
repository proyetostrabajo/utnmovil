import { HeaderComponent } from './header/header.component';
import { ItemMenuComponent } from "./item-menu/item-menu.component";
import { FooterComponent } from "./footer/footer.component";
import { CardEventComponent } from "./card/event/card-event.component";
export const SHARED_COMPONENTS = [
  HeaderComponent,
  ItemMenuComponent,
  FooterComponent,
  CardEventComponent
];

