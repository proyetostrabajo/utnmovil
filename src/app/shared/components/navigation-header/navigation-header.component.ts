import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-navigation-header',
  templateUrl: './navigation-header.component.html',
  styleUrls: ['./navigation-header.component.scss'],
})
export class NavigationHeaderComponent  implements OnInit {
  @Input() action!: string;
  @Input() title!: string;
  @Input() imgHeader!: string;
  @Input() backgroundColor!: string;
  @Input() icon!: string;
  @Output() backPressed: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    debugger;
    console.log(this.title)
    console.log(this.backgroundColor)
    // switch (this.backgroundColor) {
    //   case '/news':
    //     this.backgroundColor = 'header-news';
    //     break;
    //   case '/event':
    //     this.backgroundColor = 'header-event';
    //     break;
    //   default:
    //     // No se ha establecido ningún color
    //     break;
    // }
  }


}
