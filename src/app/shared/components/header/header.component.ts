import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent  implements OnInit {
  @Input() action!: string;
  @Input() imgHeader!: string;
  @Input() imgDiagonal!: string;
  @Input() title!: string;
  @Input() icon!: string;
  @Output() backPressed: EventEmitter<void> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.title)
  }

}
