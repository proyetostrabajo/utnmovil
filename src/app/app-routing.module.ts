import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'shell',
    pathMatch: 'full'
  },
  { path: 'shell',loadChildren: () => import('./shell/shell.module').then( m => m.ShellModule) },
  { path: 'home',loadChildren: () => import('./modules/home/home.module').then( m => m.HomeModule) },
  { path: 'news',loadChildren: () => import('./modules/news/news.module').then( m => m.NewsModule) },
  { path: 'mychannel',loadChildren: () => import('./modules/my-channel/mychannel.module').then( m => m.MychannelModule) },
  { path: 'event',loadChildren: () => import('./modules/event/page/event.module').then( m => m.EventModule) },
  {
    path: 'main',
    loadChildren: () => import('./modules/prueba/page/prueba-main.module').then(m => m.PruebaMainPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
