import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EventPage } from './main/event-main.page';
import { RouterModule } from "@angular/router";
import { SharedModule } from "../../../shared";
import { EditComponent } from "./edit/edit.component";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule, ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
        {
          path: '',  component: EventPage,

        },  {
        path: 'edit',  component: EditComponent
      }
      ]
    )
  ],
  declarations: [EventPage,EditComponent],
  exports: [RouterModule],
})
export class EventModule {}
