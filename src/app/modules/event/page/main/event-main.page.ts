import { Component, OnInit } from '@angular/core';
import { EventService } from "../../../../services/event.service";
import { ToastService } from "../../../../services/toast.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'app-main',
  templateUrl: './event-main.page.html',
  styleUrls: ['./event-main.page.scss'],
})
export class EventPage implements OnInit {
  images!:string;

  constructor(private eventService : EventService,private toastService: ToastService) { }

  ngOnInit() {
    this.images='assets/icon/eventos-header.svg';

    this.loadingEvent()
  }
  loadingEvent(){
    //getAllEvent
    this.eventService.getAllEvent().subscribe((response:any)=>{
      console.log(response);
    },(error: HttpErrorResponse)=>{
      this.toastService.presentErrorToast(error.message);
    })

  }
}
