import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MychannelPage } from './page/main/mychannel-main.page';
import { SharedModule } from "../../shared";
import { RouterModule } from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule, ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
        {
          path: '', component: MychannelPage
        }
      ]
    )
  ],
  declarations: [MychannelPage],
  exports: [RouterModule],
})
export class MychannelModule {}
