import { Component, ElementRef, OnInit } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { ToastService } from "../../../../services/toast.service";
import { ChannelService } from "../../../../services/channel.service";

@Component({
  selector: 'app-mychannel-main',
  templateUrl: './mychannel-main.page.html',
  styleUrls: ['./mychannel-main.page.scss'],
})
export class MychannelPage implements OnInit {
  backgroundColor = '#8a3838';
  constructor(private channelService : ChannelService, private toastService: ToastService,private elementRef: ElementRef) { }

  ngOnInit() {
    debugger;
    // document.getElementById('background-content').style.backgroundColor = this.backgroundColor;

    this.loading();
  }

  loading(){
    //getAllEvent
    this.channelService.getAllChannel().subscribe((response:any)=>{
      console.log(response);
    },(error: HttpErrorResponse)=>{
      this.toastService.presentErrorToast(error.message);
    })

  }
}
