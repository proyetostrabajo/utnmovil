import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MychannelMainPage } from './mychannel-main.page';

describe('MychannelMainPage', () => {
  let component: MychannelMainPage;
  let fixture: ComponentFixture<MychannelMainPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MychannelMainPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
