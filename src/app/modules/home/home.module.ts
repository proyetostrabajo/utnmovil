import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HomePage } from "./page/home.page";


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
        {
          path: '', component: HomePage
        }
      ])
  ],
  declarations: [HomePage],
  exports: [RouterModule],
})
export class HomeModule {}
