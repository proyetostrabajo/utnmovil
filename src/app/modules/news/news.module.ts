import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewsDetailPage } from "./page/detail/news-detail.page";
import { RouterModule } from "@angular/router";
import { NewsPage } from "./page/main/news-main.page";
import { SharedModule } from "../../shared/shared.module";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: NewsPage
      },
      {
        path: ':idNews', component: NewsDetailPage
      },
    ]),
    ReactiveFormsModule
  ],
  declarations: [NewsPage, NewsDetailPage],
  exports: [RouterModule],
})
export class NewsModule {}
