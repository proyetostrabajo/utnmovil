import { Component, OnInit } from '@angular/core';
import { NavController } from "@ionic/angular";

@Component({
  selector: 'app-news-main',
  templateUrl: './news-main.page.html',
  styleUrls: ['./news-main.page.scss'],
})
export class NewsPage implements OnInit {
  images!:string;
  calendarIcon!:string;
  constructor(private navController: NavController) { }

  ngOnInit() {
    this.images='assets/icon/noticias-header.svg';

    this.calendarIcon='assets/icon/materialicon-calendar_today.svg';

  }

  goToDetail(idItem: string){
    this.navController.navigateBack(['news/',idItem]);
  }

}
