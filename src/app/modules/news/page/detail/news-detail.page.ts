import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
})
export class NewsDetailPage implements OnInit {
  calendarIcon!:string;
  constructor() {
    this.calendarIcon='assets/icon/materialicon-calendar_today.svg';
  }

  ngOnInit() {
  }

}
