import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PruebaMainPage } from './main/prueba-main.page';
import { RouterModule } from "@angular/router";
import { MychannelPage } from "../../my-channel/page/main/mychannel-main.page";
import { SharedModule } from "../../../shared";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([
        {
          path: '', component: MychannelPage
        }
      ]
    )
  ],
  declarations: [PruebaMainPage],
  exports: [PruebaMainPage],
})
export class PruebaMainPageModule {}
