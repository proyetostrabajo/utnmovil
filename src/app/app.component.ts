import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "./services/auth.service";
import { ToastService } from "./services/toast.service";
import { StorageService } from "./services/storage.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService,
              private storageService: StorageService,
              private toastService: ToastService) {

  }
  ngOnInit() {
    this.doSignin();
  }
  doSignin(): void {

    const params = {
      email: 'admin@wrapper.com',
      password: 'wrapper',
    };
    //
    // this.authService.login(params).subscribe((response: any) => {
    //   this.storageService.setStorage('token-w', 'Bearer ' + response.token);
    //   this.storageService.setStorage('user-w', String(response.user.username));
    // }, (error: HttpErrorResponse) => {
    //   if(error.status === 401) {
    //     this.toastService.presentErrorToast('Usuario o contraseña incorreco');
    //   }
    //   else {
    //     this.toastService.presentErrorToast(`${error.message}`);
    //   }
    // });
  }
}
