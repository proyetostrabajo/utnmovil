import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';
import translateAuthError from './constantes';

const DEFAULT_DURATION: number = 5000;
const DEFAULT_DURATION_WARNING: number = 10000;

@Injectable({ providedIn: 'root' })
export class ToastService {

  constructor(
    public alertController: AlertController,
    public toastController: ToastController
  ) { }

  async presentErrorToast(error: HttpErrorResponse | string) {
    const message: string = typeof error === 'string' ? error : error && error.error ?
      error.error.error_description || error.error.message : 'Error';
    const toast = await this.toastController.create({
      message,
      position: 'middle',
      color: 'danger',
      duration: DEFAULT_DURATION,
    });
    toast.present();
  }

  async presentWarningToast(error: HttpErrorResponse | string) {
    const message: string = typeof error === 'string' ? error : error && error.error ?
      error.error.error_description || error.error.message : 'Error';
    const toast = await this.toastController.create({
      message,
      position: 'middle',
      color: 'warning',
      duration: DEFAULT_DURATION_WARNING,
    });
    toast.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      animated: true,
      position: 'middle',
      color: 'secondary',
      duration: DEFAULT_DURATION
    });
    toast.present();
  }
  async presentToastFirebase(mensaje:any, code:any,  position:any,  duration:any) {
    let message = '';
    if ( code === true){
      message =  mensaje;
    }else{
      message = (code) ? translateAuthError[code] : mensaje;
    }
    const toast = await this.toastController.create({
      message: message,
      position: position,
      duration: duration
    });
    toast.present();
  }
  async createAlert(header:any, backdropDismiss:any, message:any, buttonOptions1:any, buttonOptions2?:any): Promise<HTMLIonAlertElement> {
    const alert = await this.alertController.create({
      header,
      backdropDismiss,
      message,
      buttons: !buttonOptions2 ? [buttonOptions1] : [buttonOptions1,           buttonOptions2]
    });
    return alert;
  }
}
