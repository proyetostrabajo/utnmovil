import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { catchError, tap } from 'rxjs/operators';

import { StorageService } from './storage.service';
import { ToastService } from './toast.service';

@Injectable()
export class ServiceInterceptor implements HttpInterceptor {

  constructor(
    private router: Router, private toastService: ToastService,
    private authService: AuthService, private storageService: StorageService,
  ) {}

  private cloneRequest(req: HttpRequest<any>): HttpRequest<any> {
    const token = localStorage.getItem('token-w');
    let clonedRequest;

    if (token !== null) {
      clonedRequest = req.clone({ headers: req.headers.set('Authorization', token) });
    } else {
      clonedRequest = req;
    }

    return clonedRequest;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token-w');
    let clonedRequest = req;

    if (token !== null) {
      clonedRequest = this.cloneRequest(req);
    }

    return next.handle(clonedRequest).pipe(
      tap(evt => {
        if (evt instanceof HttpResponse) {
          if (evt.status === 401) {
            localStorage.clear();
            this.router.navigate(['auth/login']);
          }
          if (evt.status === 403) {
            this.toastService.presentErrorToast(`${evt.body['hydra:description']}`);
          }
        }
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.clear();
          this.router.navigate(['auth/login']);
        } else if (error.status === 403) {
          this.toastService.presentErrorToast(`${error.error['hydra:description']}`);
        }
        return throwError(error);
      })
    );
  }
}
