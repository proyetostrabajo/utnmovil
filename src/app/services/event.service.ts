import { Injectable } from '@angular/core';
import { ApiService } from "../core/api.service";
import { map, Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EventService {
  eventPath = 'utn/eventos';
  constructor(private apiService: ApiService) { }

  getAllEvent(params?:any): Observable<any> {
    return this.apiService.get(environment.url.private, this.eventPath, params)
      .pipe(
        map((response) => response.payload)
      );
  }
}
