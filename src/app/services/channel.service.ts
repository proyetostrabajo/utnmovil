import { Injectable } from '@angular/core';
import { ApiService } from "../core/api.service";
import { map, Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  channelPath = 'utn/canales';
  constructor(private apiService: ApiService) { }

  getAllChannel(params?:any): Observable<any> {
    return this.apiService.get(environment.url.private, this.channelPath, params)
      .pipe(
        map((response) => response.payload)
      );
  }
}
