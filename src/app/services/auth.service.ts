import { Injectable } from '@angular/core';
import { map, Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { ApiService } from "../core/api.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authPath = 'login_check';
  constructor(private apiService: ApiService) { }

  login(params:any): Observable<any> {
    return this.apiService.post(environment.url.private, this.authPath, params,{})
      .pipe(
        map((response) => response)
      );
  }
}
