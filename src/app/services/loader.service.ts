import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

export interface LoaderState {
  show: boolean;
}

@Injectable({ providedIn: 'root' })
export class LoaderService {
  isLoading: boolean = false;

  constructor(
    public loadingController: LoadingController
  ) { }

  async show(id: string) {
    const loading = await this.loadingController.create({
      id,
      spinner: 'lines',
      animated: false,
      mode: 'ios'
    });
    this.isLoading = true;
    return loading.present();
  }

  async hide(id: string) {
    await this.loadingController.dismiss(undefined, undefined, id);
    this.isLoading = false;
  }

}
