import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }


  getStorage(param:any) {
    let elementStorage : string | null;
    elementStorage = localStorage.getItem(param);
    if (elementStorage === null) {
      elementStorage = '';
    }
    return elementStorage;
  }

  setStorage(item: string, value: string) {
    localStorage.setItem(item, value);
  }

  removeItemStorage(param: string){
    localStorage.removeItem(param);
  }

  removeStorage(){
    localStorage.clear();
  }
}
