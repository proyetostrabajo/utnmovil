import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { Shell } from './shell.service';
import { ShellComponent } from './shell.component';

const routes: Routes = [

  { path: 'shell/', redirectTo: 'shell', pathMatch: 'full' },
  {
    path: '',
    component: ShellComponent,
    children: [
    ],
  },
];

Shell.childRoutes(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ShellRoutingModule {
}


