import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShellComponent } from "./shell.component";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { ShellRoutingModule } from "./shell-routing.module";
import { SharedModule } from "../shared";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    ShellComponent,
  ],
  imports: [
    FormsModule, ReactiveFormsModule,
    CommonModule,
    RouterModule,
    IonicModule,
    ShellRoutingModule,
    SharedModule,
  ]
})
export class ShellModule { }
