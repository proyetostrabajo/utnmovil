import { Component, OnInit } from '@angular/core';
import { NavController } from "@ionic/angular";

export interface Item{
  title: string;
  detail: string;
  url: string;
  urlImg: string;
}
@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
  itemMenu: Array<Item> = [];
  urlImgHeader!: string;
  imgDiagonal!: string;
  constructor(private navController: NavController,) {
  }

  ngOnInit() {
    this.urlImgHeader = 'assets/icon/utnsf.svg';
    this.imgDiagonal = 'assets/icon/diagonales.svg';
    this.itemMenu = [
      {
        title:'noticias',
        detail: 'No te pierdas ninguna info.',
        url: 'news',
        urlImg:'assets/icon/noticias.svg',
      },
      {
        title:'eventos',
        detail: 'añadilos a tu agenda.',
        url: 'event',
        urlImg:'assets/icon/eventos.svg',
      },
      {
        title:'turnos',
        detail: 'de exámenes.',
        url: 'mychannel',
        urlImg:'assets/icon/examenes.svg',
      },
      {
        title:'mis canales',
        detail: '',
        url: 'mychannel',
        urlImg:'assets/icon/canales.svg',
      },
    ]
  }

  goToNavigate(item:any){
    this.navController.navigateBack([item.url]);

  }

}
